# MB90A_simulation

## Robot Description :
* 4 WD Mecanum holonimic robot
* 4 ultrasonics range sensors
* 5 ir sensors
* Realsense depth camera
* lidar 
* IMU

## Included Packages 

### mb90a_simulation : 

* Robots model with sensors transform.
* Simulation (sensors-motion)
* Mapping  ***gmapping***
* Localization ***adaptive monte carlo***
* Local Planners ***dwa-teb-eband***

### mb90a_teleop :

Manual control of robot, sends velocities in `x , y , theta` in m/s

### rplidar :

Connected rplidar A2 repo `roslaunch rplidar_ros rplidar.launch`


### bno005_usb_stick :

Connected IMU sensor repo `rosrun bno055_usb_stick bno055_usb_stick_node`

### mb90a_controller:

Package includes the custom msgs to communicate with microcontroller

## Run rosserial to get sensor data :

***mb90a_simulation repo must be sourced `cd mb90a_simulation; source ./devel/setup.bash`***
* `ros_lib` libraries are inluded in `/arduino-1.8.13/libraries`, to make new libraries `rosrun rosserial_client make_libraries ~/arduino-1.8.13/libraries`
* Run rosserial with port /dev/ttyACM1 baudrate:500000  `rosrun rosserial_python serial_node.py _port:=/dev/ttyACM1 _baud:=50000`


## Run docker image
`sudo docker run -it --net=host --privileged marsesrobotics/mb90a_image /bin/bash -c "source ./devel/setup.bash;roslaunch mb90a_bringup startup.launch"`

