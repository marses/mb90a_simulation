#!/bin/sh
echo "------------------------------------- " &&
echo "---------------MB90A----------------- " &&
echo "---------------MARSES---------------- " &&
echo "This bash script shows pc info - Connected devices - Ros version and installed packages - Docker images " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
sleep 3s &&
echo "     " &&
sleep 1s &&
echo "OS info " &&
echo "     " &&
sleep 1s &&
lsb_release -a &&
echo "     " &&
sleep 1s &&
cat /etc/os-release && 
echo "     " &&
sleep 1s &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo " Ros distro " &&
echo "     " &&
sleep 1s &&
rosversion -d &&
echo "     " &&
sleep 1s &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo " Installed Ros packages " &&
echo "     " &&
sleep 1s &&
rospack list &&
echo "     " &&
sleep 1s &&  
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo " Connected USBs " &&
echo "     " &&
sleep 1s &&
lsusb && 
echo "     " && 
sleep 1s &&
if [ "ls /dev/ttyACM*" ]; then
  ls /dev/ttyACM* 
else 
     echo "No ACM devices connected"
fi 
echo "     " &&
sleep 1s &&
if [ "ls /dev/ttyUSB*" ]; then
  ls /dev/ttyUSB* 
else 
     echo "No USB devices connected"
fi 
echo "     " &&
sleep 1s &&
if [ "ls /dev/video*" ]; then
  ls /dev/video* 
else 
     echo "No Video devices connected"
fi 
echo "     " &&
sleep 1s &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo "------------------------------------- " &&
echo " Docker images  " &&
echo "     " &&
sleep 1s &&
echo '12345' | sudo -S docker images

