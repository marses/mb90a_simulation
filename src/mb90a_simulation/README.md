# ROS simulation for mb90a mobile robot

This simulation package provides a quick and easy way to try out the autonomous mobile robot from mb90a. It comes with the most commonly used configuration but is open for any kind of modification.


## MB90A Model gazebo : 

![MB90A_model](src/mb90a_simulation/mb90a_office_env.jpg)


## MB90A Rviz : 

![MB90A_rviz](src/mb90a_simulation/MB90A_rviz.png)


## Runnig Simulation :

* Gazebo simulation `roslaunch mb90a_simulation mb90a_simulation_basic.launch`
* Rviz `roslaunch mb90a_simulation mb90a_rviz.launch`
* gmapping `roslaunch mb90a_simulation mb90a_gmapping.launch`
* move_base `roslaunch  mb90a_simulation mb90a_move_base.launch`
* amcl `roslaunch  mb90a_simulation mb90a_amcl.launch`

## Available Local planners :

Change planner in ***/config/mb90a/move_base/mb90a_move_base.launch*** file

* [EBand Local Planner](http://wiki.ros.org/eband_local_planner "EBand Local Planner")
* [Teb Local Planner](http://wiki.ros.org/teb_local_planner "Teb Local Planner")
* [DWA Local Planner](http://wiki.ros.org/dwa_local_planner "DWA Local Planner")
