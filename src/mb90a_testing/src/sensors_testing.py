#!/usr/bin/env python

import rospy
from mb90a_controller.msg import Encoders
from mb90a_controller.msg import Motors
from sensor_msgs.msg import Range
from std_msgs.msg import Int32
from std_msgs.msg import Float32
from sensor_msgs.msg import Imu

from pynput.keyboard import Key, Listener,KeyCode
from time import sleep



msg = """
Testing sensors connected!
---------------------------
0 : encoders / motors velocity
1 : Ultrasonic 1
2 : Ultrasonic 2
3 : Ultrasonic 3 
4 : Ultrasonic 4
5 : IR 1
6 : IR 2
7 : IR 3
8 : IR 4
9 : IR 5
e : Emergency
v : Voltage
l : Toggle led
i : IMU

d : Sensors information
m : Sensors Models
CTRL-C to quit
"""

e = """
Communications Failed
"""
sen = """
Intel Real sense d415
Ultrasonic US-100
Sharp Ir 2Y0A21 F 91
Bosh IMU Bno055 usb stick
"""

comb=Key
COMBINATION = {KeyCode.from_char('c'), Key.ctrl}
current = set()

button = ""
last_button = ""
rate=0.1

def on_press(key):
    global button
    button=format(key)[2]
    if key in COMBINATION:
        current.add(comb)
        if all(k in current for k in COMBINATION):
            rate=rate

def on_release(key):
    if key == Key.esc:
        return False
    if key in COMBINATION:
        if all(k in current for k in COMBINATION):
            return False


liste = Listener(on_press=on_press, on_release=on_release)
liste.start()

us1=us2=us3=us4=ir1=ir2=ir3=ir4=ir5=volt=emergency=0


us_frame_id=us_field_of_view=us_min_range=us_max_range=ir_frame_id=ir_field_of_view=ir_min_range=ir_max_range=0

debug_imu=0

m1=m2=m3=m4=0

def ultrasonic_leftCallback (msg):
    global us1,us_frame_id,us_field_of_view,us_min_range,us_max_range
    us1 = msg.range    
    us_frame_id = msg.header.frame_id
    us_field_of_view = msg.field_of_view
    us_min_range = msg.min_range
    us_max_range = msg.max_range
def ultrasonic_front_leftCallback (msg):
    global us2
    us2=msg.range
def ultrasonic_front_rightCallback (msg):
    global us3
    us3=msg.range
def ultrasonic_rightCallback (msg):
    global us4
    us4=msg.range
def ir_leftCallback (msg):
    global ir1,ir_frame_id,ir_field_of_view,ir_min_range,ir_max_range
    ir1=msg.range
    ir_frame_id = msg.header.frame_id
    ir_field_of_view = msg.field_of_view
    ir_min_range = msg.min_range
    ir_max_range = msg.max_range
def ir_front_leftCallback (msg):
    global ir2
    ir2=msg.range
def ir_front_rightCallback (msg):
    global ir3
    ir3=msg.range
def ir_rightCallback (msg):
    global ir4
    ir4=msg.range
def ir_backCallback (msg):
    global ir5
    ir5=msg.range
def voltCallback (msg):
    global volt
    volt=msg.data
def emergencyCallback (msg):
    global emergency
    emergency=msg.data
def imuCallback (msg):
    global debug_imu
    if debug_imu == 1 :
        print("orientation : \n ----------------------- ")
        print(str(msg.orientation)+"\n -----------------------")
        print("angular_velocity : \n -----------------------")
        print(str(msg.angular_velocity)+" \n -----------------------")
        print("linear_acceleration : \n -----------------------")
        print(str(msg.linear_acceleration)+" \n -----------------------")
def speedCallback(msg):
    global m1,m2,m3,m4
    m1=msg.enc1
    m2=msg.enc2
    m3=msg.enc3
    m4=msg.enc4

if __name__=="__main__":
    rospy.init_node("sensors_testing")
    print(msg)

    
    rospy.Subscriber("/ultrasonic_left", Range, ultrasonic_leftCallback)
    rospy.Subscriber("/ultrasonic_front_left", Range, ultrasonic_front_leftCallback)
    rospy.Subscriber("/ultrasonic_front_right", Range, ultrasonic_front_rightCallback)
    rospy.Subscriber("/ultrasonic_right", Range, ultrasonic_rightCallback)
    rospy.Subscriber("/ir_left", Range, ir_leftCallback)
    rospy.Subscriber("/ir_front_left", Range, ir_front_leftCallback)
    rospy.Subscriber("/ir_front_right", Range, ir_front_rightCallback)
    rospy.Subscriber("/ir_right", Range, ir_rightCallback)
    rospy.Subscriber("/ir_back", Range, ir_backCallback)
    rospy.Subscriber("/voltage_sensor", Float32, voltCallback)
    rospy.Subscriber("/emergency_button", Int32, emergencyCallback)
    rospy.Subscriber("/imu", Imu, imuCallback)

    rospy.Subscriber("/motor_speeds", Encoders, speedCallback)

    ledPub = rospy.Publisher("/led", Int32, queue_size=10)





    while not rospy.is_shutdown():
        if button == "0":
            print("")
            print ("Showing Encoders :")
            print ("-------------------------------")
            while button == "0" :
                print("M1 : " + str(m1) + " m/s")
                print("M2 : " + str(m2) + " m/s")
                print("M3 : " + str(m3) + " m/s")
                print("M4 : " + str(m4) + " m/s")
                print ("-------------------------------")
                sleep(rate)
        if button == "1":
            print("")
            print ("Showing Ultrasonic left")
            print ("-------------------------------")
            while button == "1" :
                print("US 1 Range : " + str(us1))
                sleep(rate)
        if button == "2":
            print("")
            print ("Showing Ultrasonic front left")
            print ("-------------------------------")
            while button == "2" :
                print("US 2 Range : " + str(us2))
                sleep(rate)

        if button == "3":
            print("")
            print ("Showing Ultrasonic front right :")
            print ("-------------------------------")
            while button == "3" :
                print("US 3 Range : " + str(us3))
                sleep(rate)

        if button == "4":
            print("")
            print ("Showing Ultrasonic right :")
            print ("-------------------------------")
            while button == "4" :
                print("US 4 Range : " + str(us4))
                sleep(rate)

        if button == "5":
            print("")
            print ("Showing Ir left :")
            print ("-------------------------------")
            while button == "5" :
                print("Ir 1 Range : " + str(ir1))
                sleep(rate)
        if button == "6":
            print("")
            print ("Showing Ir front left :")
            print ("-------------------------------")
            while button == "6" :
                print("Ir 2 Range : " + str(ir2))
                sleep(rate)
        if button == "7":
            print("")
            print ("Showing Ir front right :")
            print ("-------------------------------")
            while button == "7" :
                print("Ir 3 Range : " + str(ir3))
                sleep(rate)
        if button == "8":
            print("")
            print ("Showing Ir right :")
            print ("-------------------------------")
            while button == "8" :
                print("Ir 4 Range : " + str(ir4))
                sleep(rate)
        if button == "9":
            print("")
            print ("Showing Ir back :")
            print ("-------------------------------")
            while button == "9" :
                print("Ir 5 Range : " + str(ir5))
                sleep(rate)
        if button == "e":
            print("")
            print ("Showing Emergency button :")
            print ("-------------------------------")
            while button == "e" :
                print("Emergency button State : " + str(emergency))
                sleep(rate)

        if button == "v":
            print("")
            print ("Showing Battery Voltage :")
            print ("-------------------------------")
            while button == "v" :
                print("Volt : " + str(volt))
                sleep(rate)

        if button == "l":
            print("")
            print ("Toggle LEDS :")
            print ("-------------------------------")
            while button == "l" :
                print()
                sleep(rate)
        if button == "i":
            print("")
            print ("Toggle LEDS :")
            print ("-------------------------------")
            while button == "i" :
                debug_imu=1
                sleep(rate)
            debug_imu=0

        if button == "d" and last_button != button:
            print("")
            print ("Print sensors info :")
            print ("-------------------------------")
            print ("Ultrasonics sensors info :")
            print ("-------------------------------")
            print("  us_frame_id : " +  str(us_frame_id) + "  us_field_of_view : " + str(us_field_of_view) + "  us_min_range : " + str(us_min_range) + "  us_max_range : " + str(us_max_range))
            print("")
            print ("Irs sensors info :")
            print ("-------------------------------")
            print("  ir_frame_id : " + str(ir_frame_id)+"  ir_field_of_view : " + str(ir_field_of_view) + "  ir_min_range : "   + str(ir_min_range) + "  ir_max_range : " + str(ir_max_range))

        if button == "m" and last_button != button:
            print("")
            print ("Sensors Models :")
            print ("-------------------------------")
            print(sen)

        else :
            if last_button != button and button != "d" and button != "m"  :
                print(msg)
        last_button=button
        sleep(rate)