# MB90A Testing peripherals node

This package is made to test the connected peripherals.
- Startup code should be running from docker image 
- Or rosserial_python to get data from microcontroller `rosrun rosserial_python serial_node.py /dev/ttyACM1`

## Sensors Connected :
    encoders / motors velocity
    Ultrasonic 1
    Ultrasonic 2
    Ultrasonic 3 
    Ultrasonic 4
    IR 1
    IR 2
    IR 3
    IR 4
    IR 5
    Emergency
    Voltage
    Toggle led
    IMU
    Sensors information
    Sensors Models

## Visualize data on rviz 

To run visualization, docker startup must be running.

`roslaunch mb90a_testing mb90a_testing_visualization.launch` 
