# mb90a_teleop


* Run teleop node `roslaunch mb90a_teleop teleop_keyboard.launch`

    keyboard Control
    ---------------------
    Use 'WASD' to translate
    Use 'QE' to yaw
    Press 'Shift' to run

* To change velocities open teleop_keyboard.launch file change parameters 
- param "walk_vel"         default_value "0.25"    
- param "run_vel"          default_value "0.4"   
- param "yaw_rate"         default_value "0.6" 
- param "yaw_run_rate"     default_value "0.8" 

